<?php get_header(); ?>

<!-- BANNER -->

		 <?php include('module/banner.php'); ?>


		<!-- CONTENT -->

		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">
		<div class="col-sm-12">
				<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>

			</div>
			<div class="col-sm-3"></div>

				<div class="col-sm-6">
<?php get_search_form(); ?>
			</div>
				<div class="col-sm-3"></div>
				<div class="clearfix"></div>
			<div class="col-sm-8">
			<div class="main">

				<h3 class="search-title" style="margin-top:20px;">
						<?php echo $wp_query->found_posts; ?> <?php _e( 'Search Results Found For', 'locale' ); ?>: "<?php the_search_query(); ?>"
					</h3>
				 	<?php if ( have_posts() ) : ?>

				 	<ol class="list-unstyled list-archives">

					<!-- Loop -->
					<?php while ( have_posts() ) : the_post(); ?>

						<li>

							<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>

							<div class="meta">Posted: <?php the_time('F j Y') ?></div>

							<?php the_excerpt(); ?>
						</li>

					<?php endwhile; ?>

				 	</ol>

				 	<div class="row">
				 		<div class="navprevious col-xs-6"><?php next_posts_link( '&laquo; Older ' ); ?></div>
				 		<div class="navnext col-xs-6"><?php previous_posts_link( 'Newer &raquo;' ); ?></div>
				 	</div>

			        <?php else : ?>

			            <p>No posts available</p>

			        <?php endif; ?>
					<!-- END Loop -->
			</div>
			</div><!--/.main -->


		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->

<?php get_footer(); ?>
