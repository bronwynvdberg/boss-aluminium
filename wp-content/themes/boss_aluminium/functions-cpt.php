<?php /** Custom Post Type **/
	add_action( 'init', 'create_post_type' );
	function create_post_type() {
		/** Create Slider **/
	  register_post_type( 'slider',
	    array(
	      'labels' => array(
	        'name' => __( 'Slider' ),
	        'singular_name' => __( 'Slider' ),
					'add_new'            => _x( 'Add New', 'Slider' ),
	    		'add_new_item'       => __( 'Add New Slider' ),
	    		'edit_item'          => __( 'Edit Slider' ),
		    	'new_item'           => __( 'New Slider' ),
		    	'all_items'          => __( 'All Slider' )
						),
			      'public' => true,
			      'has_archive' => false,
						'menu_icon' => 'dashicons-images-alt2',
						'supports' => array( 'title', 'editor', 'thumbnail' )
			    )
	  );
		/** Create Showcase Display **/
		register_post_type( 'Showcase',
			array(
				'labels' => array(
					'name' => __( 'Showcase' ),
					'singular_name' => __( 'Showcase' ),
					'add_new'            => _x( 'Add New', 'Showcase' ),
					'add_new_item'       => __( 'Add New Showcase' ),
					'edit_item'          => __( 'Edit Showcase' ),
					'new_item'           => __( 'New Showcase' ),
					'all_items'          => __( 'All Showcase' )
						),
						'public' => true,
						'has_archive' => false,
						'menu_icon' => 'dashicons-images-alt',
						'supports' => array( 'title', 'thumbnail' )
					)
		);
		/** Create FAQ **/
		register_post_type( 'faq',
			array(
				'labels' => array(
					'name' => __( 'FAQ' ),
					'singular_name' => __( 'FAQ' ),
					'add_new'            => _x( 'Add New', 'FAQ' ),
					'add_new_item'       => __( 'Add New FAQ' ),
					'edit_item'          => __( 'Edit FAQ' ),
					'new_item'           => __( 'New FAQ' ),
					'all_items'          => __( 'All FAQ' )
						),
						'public' => true,
						'has_archive' => false,
						'menu_icon' => 'dashicons-editor-help',
						'supports' => array( 'title', 'editor', 'thumbnail' )
					)
		);
		/** Create Gallery Display **/
	register_post_type( 'Gallery',
		array(
			'labels' => array(
				'name' => __( 'Gallery' ),
				'singular_name' => __( 'Photo' ),
				'add_new'            => _x( 'Add New', 'Photo' ),
				'add_new_item'       => __( 'Add New Photo' ),
				'edit_item'          => __( 'Change Photo' ),
				'new_item'           => __( 'New Photo' ),
				'all_items'          => __( 'All Photos' )
					),
					'public' => true,
					'has_archive' => false,
					'menu_icon' => 'dashicons-format-gallery',
					'supports' => array( 'title', 'thumbnail' )
				)
	);
}

/*** register field groupd for Slider post type ***/
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_button',
			'title' => 'button',
			'fields' => array (
				array (
					'key' => 'field_button_text',
					'label' => 'button text',
					'name' => 'button_text',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_button link',
					'label' => 'button link',
					'name' => 'button_link',
					'type' => 'text',
					'instructions' => 'Copy and paste the link url to this field',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'showcase',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}



	/*** register field groupd for Gallery post type ***/
		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_video_url',
				'title' => 'video',
				'fields' => array (
					array (
						'key' => 'field_video_url',
						'label' => 'Video URL',
						'name' => 'video_url',
						'type' => 'text',
						'instructions' => 'Copy & Paste the video url in this field, the popup lightbox will display the video player. Currently support Youtube & Vimeo.',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'gallery',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}

?>