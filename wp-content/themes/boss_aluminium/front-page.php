<?php

/*
Template Name: Page - Home
*/

get_header(); ?>

		<!-- SLIDER -->
		<?php //include('module/slider.php'); ?>


		<!-- SHOWCASE -->
		<?php //include('module/showcase.php'); ?>


		<!-- CONTENT -->

		<div class="content">
			<div class="container">
				<div class="contentwrap">
					<div class="row">

						<div class="col-sm-8">
							<div class="main">
								<!-- Loop -->
								<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
									<?php the_content(); ?>
								<?php endwhile; ?>
								<!-- END Loop -->
							</div>
						</div>
						<div class="col-sm-4">
							<div class="aside">
								<?php //get_sidebar(); ?>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="bottom-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-6"><?php // include('module/testimonial-slider.php'); ?></div>
					<div class="col-sm-6"><?php // echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="false"]'); ?></div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
