
--== SBM-Framework ==--


> Sticky Footer
  # The page wrapper needs to have a 'padding-bottom' which matches the footer 'height' and 'margin-top'.
  @ Adjust:
    - '.wrap' > 'padding-bottom'
    - '.footer' > 'margin-top'
    - '.footer .container' > 'height'



