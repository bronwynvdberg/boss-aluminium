<?php
/*
* Template Name: Page - Contact
*/

 get_header(); ?>

		 <?php include('module/map.php'); ?>

		<!-- CONTENT -->

		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">

			<div class="col-sm-8">
			<div class="main">

				<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>

				<!-- Loop -->
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

					<h1><?php the_title(); ?></h1>

					<?php the_content(); ?>

				<?php endwhile; ?>
				<!-- END Loop -->
        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="false"]'); ?>
			</div>
			</div><!--/.main -->

			<div class="col-sm-4">
			<div class="aside">

				<?php get_sidebar(); ?>

			</div>
			</div><!--/.aside -->

		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->

<?php get_footer(); ?>
