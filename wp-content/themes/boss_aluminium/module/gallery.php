<div class="row gallery-grid">
<?php		$args = array( 'post_type' => 'gallery');
        $loop = new WP_Query( $args ); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); 	?>
  <?php
  $image_full = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  $image_medium = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
  ?>
  <div class="col-sm-3">
      <?php
        $video_url = get_field('video_url');
        if (!empty($video_url)){ ?>
        <a href="<?php echo $video_url; ?>" rel="lightbox" title="<?php the_title(); ?>" class="swipebox-video">
          <div class="showcaseimage" style="background-image:url(<?php echo $image_medium[0]; ?>); ">
          </div>
        </a>
      <?php }else{ ?>
          <a href="<?php echo $image_full[0]; ?>" rel="lightbox" title="<?php the_title(); ?>" class="swipebox">
            <div class="showcaseimage" style="background-image:url(<?php echo $image_medium[0]; ?>); ">

            </div>
          </a>
        <?php }?>
    </div>
  <?php endwhile;?>
</div>
