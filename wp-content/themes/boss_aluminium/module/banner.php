<!-- BANNER -->

<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<?php endif; ?>
<!-- BANNER -->
<div class="bannerwrap">
<div class="banner" style="background-image:url('<?php if(has_post_thumbnail()){echo $image[0];}else{echo bloginfo('template_url')."/assets/images/banners/banner-sm.jpg";} ?>')">
</div><!--/.banner -->
</div><!--/.bannerwrap -->
