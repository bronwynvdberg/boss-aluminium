<?php
/*
* Template Name: Thank You Page
*/
get_header(); ?>

	 <?php include('module/banner.php'); ?>

		<!-- CONTENT -->

		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">

			<div class="col-sm-8">
			<div class="main">
				<!-- Loop -->
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php
          $name = htmlspecialchars($_GET["clientname"]) ;
          ?>
  				<h4> Dear <?php echo $name; ?>,  </h4>
				<h4>	<?php the_content(); ?>  </h4>

				<?php endwhile; ?>
				<!-- END Loop -->

			</div>
			</div><!--/.main -->

			<div class="col-sm-4">
			<div class="aside">

				<?php get_sidebar(); ?>

			</div>
			</div><!--/.aside -->

		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->

<?php get_footer(); ?>
