<?php

	// Hide Top Admin Panel
	add_filter('show_admin_bar', '__return_false');

	// Menus - Register //
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'clickforward' ),
		'footer' => __( 'Footer Navigation', 'clickforward' ),
	) );

	// Menus - Add Active Class //

	function current_to_active($text){
		$replace = array(
			'current_page_item' => 'active',
			'current_page_parent' => 'active',
			'current_page_ancestor' => 'active',
		);
		$text = str_replace(array_keys($replace), $replace, $text);
			return $text; }
	add_filter ('wp_nav_menu','current_to_active');

	// Widgets //

	function clickforward_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'clickforward' ),
		'id' => 'sidebar',
		'before_widget' => '<div class="panel panel-default panel-%1$s %2$s">',
		'after_widget' => '</div></div>',
		'before_title' => '<div class="panel-heading"><h3 class="panel-title">',
		'after_title' => '</h3></div><div class="panel-body">',
	) );
	register_sidebar( array(
		'name' => __( 'Showcase', 'clickforward' ),
		'id' => 'showcase',
		'before_widget' => '<div class="col-sm-4"><div class="panel panel-default panel-%1$s %2$s">',
		'after_widget' => '</div></div></div>',
		'before_title' => '<div class="panel-heading"><h3 class="panel-title">',
		'after_title' => '</h3></div><div class="panel-body">',
	) );
	}

	add_action( 'widgets_init', 'clickforward_widgets_init' );

	// Format navbar, wpadminbar and page content correctly //

	function admin_bar_offset() {
		echo"<style>@media (max-width:768px){body{margin-top:0px}</style>";

		if(is_admin_bar_showing()) {
			echo"<style>.navbar-fixed-top.visible-xs {top:0px;} @media (max-width:768px){ #wpadminbar {display:none !important;} html{margin-top:0 !important} }</style>";
		}
	}

	add_action('wp_head', 'admin_bar_offset');

	// Remove Default Gallery Style //

	add_filter( 'use_default_gallery_style', '__return_false' );

	// Enable Post Thumbnails //

	add_theme_support( 'post-thumbnails' );

	// Admin - Style //

	add_action('admin_head', 'trublue_admin_style');

	function trublue_admin_style() {
	echo '<style>
		#adminmenu {
		margin: 0;
		}
		#adminmenu div.separator {
		border-color: #444;
		}
		#adminmenu li.wp-menu-separator {
		margin: 0;
		}
	}
	</style>';
	}

	add_action('admin_bar_menu', 'trublue_admin_bar');

	function trublue_admin_bar() {
	echo '<style>
		#wpadminbar {
		background: #000;
		}
		#wpadminbar #wp-admin-bar-wp-logo,
		#wpadminbar #wp-admin-bar-comments,
		#wpadminbar #wp-admin-bar-wpseo-menu,
		#wpadminbar #wp-admin-bar-slideshows-top_menu {
		display:none;
		}
	}
	</style>';
	}

	add_action('login_head', 'trublue_login_logo');
	function trublue_login_logo() {
    echo '<style type="text/css">
		.login h1 a {
		background-image:url('.get_stylesheet_directory_uri().'/assets/admin/SBM_logo.png) !important;
		background-size: 200px !important;
    	width: 100%;
    	height:110px;
    	padding-bottom: 0 !important;
		background-position: 50% 50%;
		}
    </style>';
	}

	add_filter('login_headerurl', 'trublue_login_url');
	function trublue_login_url(){
    	return get_bloginfo( 'wpurl' );
	}

	add_filter('admin_footer_text', 'rvam_modify_footer_admin');
	function rvam_modify_footer_admin ()
	{
	    echo '<span id="footer-thankyou">Website developed by <a href="http://www.smithbrothersmedia.com.au" target="_blank">Smith Brothers Media </a> | For problems please send a <a href="mailto:support@smithbrothersmedia.com.au?subject=Support Request&body=For support requests please make sure you let us know your name and contact detains, the domain name of the site, and a detailed description of the problem. Thank you." target="_blank">Support Request</a></span>';
	}

	add_filter ( 'login_errors', 'better_failed_login' );
	function better_failed_login () {
	    return 'The login information you have entered is incorrect. Please try again.';
	}

	// Admin - Redirect non-admin's //

	function nonadmin_login_redirect( $redirect_to, $request, $user  ) {
		return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url();
	}

	add_filter( 'login_redirect', 'nonadmin_login_redirect', 10, 3 );

	// Admin - Remove Menu Items //

	function remove_menus(){
		remove_menu_page( 'edit-comments.php' );
	}

	add_action( 'admin_menu', 'remove_menus' );



	// SBM CUSTOM POST TYPES
	include_once "functions-cpt.php";
	
	// SBM - Advanced Custom Fields
	include_once "functions-acf.php";

	// SBM - Custom Functions - STiTCHi
	include_once "functions-custom.php";

?>
