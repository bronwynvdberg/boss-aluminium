<?php
// Allow SVG in Media Library
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// 
add_action( 'admin_menu', 'sbmS_add_admin_menu' );
add_action( 'admin_init', 'sbmS_settings_init' );
//assets/
/*function no_wp_logo_admin_bar_remove() {
    ?>
        <style type="text/css">
            #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
                content: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sbm-logo-s2.png)   !important;
                top: 2px;
            }

            #wpadminbar #wp-admin-bar-wp-logo > a.ab-item {
                pointer-events: none;
                cursor: default;
                width: 42px;
            }
        </style>
    <?php
}
add_action('wp_before_admin_bar_render', 'no_wp_logo_admin_bar_remove', 0);
*/
/*
function sbmS_login_logo() { ?>
    <style type="text/css">

		body, html {
		    background: #3E3833 !important;
		}

		.login form {
		    margin-top: 20px;
		    margin-left: 0;
		    padding: 26px 24px 46px;
		    background: #292929 !important;
		    -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.13);
		    box-shadow: 0 1px 3px rgba(0,0,0,.13);
		}

		.wp-core-ui .button-primary {
		    background: #BB6B2E !important;
		    border-color: #BB6B2E !important;
		    -webkit-box-shadow: 0 1px 0 #BB6B2E !important;
		    box-shadow: 0 1px 0 #9a531c !important;
		    color: #fff !important;
		    text-decoration: none !important;
		    text-shadow: 0 -1px 1px #BB6B2E,1px 0 1px #BB6B2E,0 1px 1px #BB6B2E,-1px 0 1px #BB6B2E !important;
		}

		.login label {
		    color: #BB6B2E !important;
		    font-size: 14px !important;
		}

		.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
		    color: #BB6B2E !important;
		}


		.login #backtoblog a, .login #nav a {
		    color: #BB6B2E !important;
		}

        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_SBM.png);
            background-size: 165px auto !important;
			background-position: center top !important;
			background-repeat: no-repeat !important;
            padding-bottom: 50px !important;
            width:165px !important;
            height:50px !important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'sbmS_login_logo' );
*/

function sbmS_add_admin_menu(  ) {

	add_menu_page( 'sbmSettings', 'SBM Settings', 'manage_options', 'sbmsettings', 'sbmS_options_page' );

}


function sbmS_settings_init(  ) {

	register_setting( 'pluginPage', 'sbmS_settings' );

	add_settings_section(
		'sbmS_pluginPage_section',
		__( '<small>From This settings panel you are able to quickly add in escentual details for the website.</small>', 'wordpress' ),
		'sbmS_settings_section_callback',
		'pluginPage'
	);

	add_settings_field(
		'sbmS_text_field_0',
		__( "Google Analytics UA Code: e.g. UA-XXXXXXXX-X", 'wordpress' ),
		'sbmS_text_field_0_render',
		'pluginPage',
		'sbmS_pluginPage_section'
	);

	add_settings_field(
		'sbmS_text_field_remarketing',
		__( "Google Remarketing Tag Conversion ID: e.g. 0000000000", 'wordpress' ),
		'sbmS_text_field_remarketing_render',
		'pluginPage',
		'sbmS_pluginPage_section'
	);

	add_settings_field(
		'sbmS_textarea_field_1',
		__( 'Header Custom Scripts/HTML/CSS', 'wordpress' ),
		'sbmS_textarea_field_1_render',
		'pluginPage',
		'sbmS_pluginPage_section'
	);

	add_settings_field(
		'sbmS_textarea_field_2',
		__( 'Footer Custom Scripts/HTML/CSS', 'wordpress' ),
		'sbmS_textarea_field_2_render',
		'pluginPage',
		'sbmS_pluginPage_section'
	);


}
//google_conversion_id

function sbmS_text_field_0_render(  ) {

	$options = get_option( 'sbmS_settings' );
	?>
	<input type='text' name='sbmS_settings[sbmS_text_field_0]' value='<?php echo $options['sbmS_text_field_0']; ?>'>
	<?php

}

function sbmS_text_field_remarketing_render(  ) {

	$options = get_option( 'sbmS_settings' );
	?>
	<input type='text' name='sbmS_settings[sbmS_text_field_remarketing]' value='<?php echo $options['sbmS_text_field_remarketing']; ?>'>
	<?php

}

function sbmS_textarea_field_1_render(  ) {

	$options = get_option( 'sbmS_settings' );
	?>
	<textarea cols='55' rows='5' name='sbmS_settings[sbmS_textarea_field_1]'><?php echo $options['sbmS_textarea_field_1']; ?></textarea>
	<?php

}


function sbmS_textarea_field_2_render(  ) {

	$options = get_option( 'sbmS_settings' );
	?>
	<textarea cols='55' rows='5' name='sbmS_settings[sbmS_textarea_field_2]'><?php echo $options['sbmS_textarea_field_2']; ?></textarea>
	<?php

}


function sbmS_settings_section_callback(  ) {

	//echo __( 'This section description', 'wordpress' );

}


function sbmS_options_page(  ) {

	?>
    <!-- Create a header in the default WordPress 'wrap' container -->
    <div class="wrap">

        <div id="icon-themes" class="icon32"></div>
        <h2>SBM Settings Panel</h2>
        <?php settings_errors(); ?>

        <?php
        $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'display_general';
        ?>

        <h2 class="nav-tab-wrapper">
            <a href="?page=sbmsettings&tab=display_general" class="nav-tab <?php echo $active_tab == 'display_general' ? 'nav-tab-active' : ''; ?>">General</a>
            <a href="?page=sbmsettings&tab=display_more" class="nav-tab <?php echo $active_tab == 'display_more' ? 'nav-tab-active' : ''; ?>">More</a>
        </h2>


	<form action='options.php' method='post'>

		<?php
		// tab 1 // General
		if( $active_tab == 'display_general' ) {

			settings_fields( 'pluginPage' );
			do_settings_sections( 'pluginPage' );
			submit_button();

		}
		else {
			// tab 2 // More

			?>
			<h2>More to come...</h2>
			<?php

submit_button();

		}
		?>


	</form>

    </div><!-- /.wrap -->



	<?php

}


add_action('wp_head', 'sbmS_header_custom_scripts', 100);

function sbmS_header_custom_scripts() {

	$options = get_option( 'sbmS_settings' );

	if($options['sbmS_textarea_field_1'] !="") {

		echo "<!-- Custom Head - SBM settings //start -->\n".$options['sbmS_textarea_field_1']."\n<!-- Custom Head - SBM settings //end -->\n";
	}


	if( $options['sbmS_text_field_0'] != ""){
?>
<!-- Google Analytics - SBM settings //start -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $options['sbmS_text_field_0']; ?>', 'auto');
  ga('send', 'pageview');

</script>
<!-- Google Analytics - SBM settings //end -->
<?php

	}

}

add_action('wp_footer', 'sbmS_footer_custom_scripts', 100);

function sbmS_footer_custom_scripts() {

	// get all the options //
	$options = get_option( 'sbmS_settings' );

	if( $options['sbmS_textarea_field_2'] != "") {
		echo "\n<!-- Custom Footer - SBM settings //start -->\n".$options['sbmS_textarea_field_2']."\n<!-- Custom Footer - SBM settings //end -->\n";
	}

	if( $options['sbmS_text_field_remarketing'] != "") {

		echo "<!-- Google Code for Remarketing Tag - SBM settings //start -->\n";

	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = <?php echo $options['sbmS_text_field_remarketing']; ?>;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/<?php echo $options['sbmS_text_field_remarketing']; ?>/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript><?php
	echo "\n<!-- Google Code for Remarketing Tag - SBM settings //end -->\n";

	}

}

?>
