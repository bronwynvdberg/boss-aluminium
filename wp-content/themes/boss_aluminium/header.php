<!DOCTYPE html>
<html lang="en">
<head>

    <title><?php wp_title(); ?></title>

    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <!-- Icons -->
    <link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
    <link rel="apple-touch-startup-image" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-touch-icon-57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-touch-icon-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-touch-icon-114.png" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?a=03" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/swipebox.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/jasny-bootstrap.min.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.placeholder.js"></script>
    <![endif]-->

    <!--  IE @media fix -->
    <script>
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement("style")
      msViewportStyle.appendChild(
        document.createTextNode(
          "@-ms-viewport{width:auto!important}"
        )
      )
      document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
    }
    </script>

    <!-- Wordpress Header -->
    <?php wp_head(); ?>

</head>

<body <?php body_class($class); ?>>
  <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
  <![endif]-->

  <a href="#content" class="sr-only">Skip to main content</a>

  <div class="wrap">
    <header class="fixed-top">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
              <img src="http://ph4se.com.au/image.php?text=Organisation Logo&w=300&h=80&size=20">
            </a>
            <div id="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="collapsed">
              <span></span><span></span><span></span>
            </div>
            <div class="navbar-collapse collapse" id="navbarCollapse" style="">
              <?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '<ul class="navbar-nav">%3$s</ul>', 'container' => false) ); ?> 
            </div>
        </div>
      </nav>
    </header>
    <?php wp_reset_postdata(); wp_reset_query(); ?>
     