<?php
/*
*  Template Name: Page - Gallery Page
*/
get_header(); ?>

		 <?php include('module/banner.php'); ?>

		<!-- CONTENT -->

		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">

			<div class="col-sm-12">
			<div class="main">

				<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>

				<!-- Loop -->
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

					<h1><?php the_title(); ?></h1>

					<?php the_content(); ?>

				<?php endwhile; ?>
				<!-- END Loop -->
        <hr>
        <?php include('module/gallery.php'); ?>

			</div>
			</div><!--/.main -->



		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->

<?php get_footer(); ?>
