<?php
/*
		Advanced Custom Fields

		- CONTACT PAGE
		  - Phone, Address, Email
		  - Facebook, Twitter, Instagram, Youtube, Snapchat, Linked In
		  - Location



*/
	// Page IDs
	$contact_page = 9;


	/*
				CONTACT INFORMATION

	*/
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_contact',
			'title' => 'Contact',
			'fields' => array (
				array (
					'key' => 'field_5a2622e70cf2b',
					'label' => 'Contact Details',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_5a2623120cf2e',
					'label' => 'Phone',
					'name' => 'phone',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a2622f80cf2c',
					'label' => 'Address',
					'name' => 'address',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a2623030cf2d',
					'label' => 'Email',
					'name' => 'email',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a2623170cf2f',
					'label' => 'Social',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_5a2623328352b',
					'label' => 'Facebook',
					'name' => 'facebook',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a2623388352c',
					'label' => 'Twitter',
					'name' => 'twitter',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a26233d8352d',
					'label' => 'Instagram',
					'name' => 'instagram',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a8e1b742e549',
					'label' => 'Youtube',
					'name' => 'youtube',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a2623488352e',
					'label' => 'Snapchat',
					'name' => 'snapchat',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5aa1d7439aba2',
					'label' => 'Linked In',
					'name' => 'linked_in',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5a262362e627d',
					'label' => 'Map',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_5a262369e627e',
					'label' => 'Location',
					'name' => 'location',
					'type' => 'google_map',
					'center_lat' => '',
					'center_lng' => '',
					'zoom' => '',
					'height' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'page',
						'operator' => '==',
						'value' => $contact_page,
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}


?>