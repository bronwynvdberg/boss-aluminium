<div style="color:#F00;">
<?php
	globals_output();
?>
</div>


<?php



	$contact_id = 13;


	// $hero_text = get_field('hero_text', $contact_id);
	// $hero_image = get_field('hero_image', $contact_id);
	// $hero_size = get_field('hero_size', $contact_id);
	// $hero_position = get_field('hero_position', $contact_id);
	// $hero_colour = get_field('hero_overlay_colour', $contact_id);
	// $hero_opacity = get_field('hero_overlay_opacity', $contact_id);

	// $footer_copyright = get_field('footer_copyright', $contact_id);

	// $facebook = get_field('facebook', $contact_id);	
	// $instagram = get_field('instagram', $contact_id);	
	// $twitter = get_field('twitter', $contact_id);	
	// $snapchat = get_field('snapchat', $contact_id);	

	$facebook = "#";
	$instagram = "#";
	$twitter = "#";
	$snapchat = "#";


	// CacheBuster
	// $version = time();


?>

		<!-- FOOTER -->
		<div class="col-xs-12 col-md-12 footer-bg-banner" style="background-image: url('<?=$hero_image['url']?>'); background-size:<?=$hero_size?>; background-position:<?=$hero_position?>">
			<div style="background-color:<?=$hero_colour?>; opacity:<?=$hero_opacity?>"></div>
			<div class="hero-content">
				<?=$hero_text?>
			</div>
		</div>
		
		<footer>
			<div class="footer">
				<div class="container">
					<div class="footerwrap">
						<div class="row">
							<div class="col-sm-3 footer-logo">
								<img src="http://ph4se.com.au/image.php?text=Organisation Logo&w=300&h=80&size=20">
							</div>
							<div class="col-sm-6">
								<?=$footer_copyright?>
							</div>
							<div class="col-sm-3">
								<div class="row">
									<div class="col-sm-8 footer-social">
										<?php
											if (!is_null($facebook) && !empty($facebook)) {
												echo "<a href=\"{$facebook}\" class=\"sbm-icon_facebook\"></a>";
											}
											if (!is_null($instagram) && !empty($instagram)) {
												echo "<a href=\"{$instagram}\" class=\"sbm-icon_instagram\"></a>";
											}
											if (!is_null($twitter) && !empty($twitter)) {
												echo "<a href=\"{$twitter}\" class=\"sbm-icon_twitter\"></a>";
											}
											if (!is_null($snapchat) && !empty($snapchat)) {
												echo "<a href=\"{$snapchat}\" class=\"sbm-icon_snapchat\"></a>";
											}
										?>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</footer>
	</div>

</div>






	<footer>
		<div class="site-copyright">
			<div class="container">
				<p>
					&copy; <?php echo date("Y")." ".get_bloginfo('name'); ?>. All Rights reserved <span>|</span>
					<a href="#privacy">Privacy Policy</a> <span>|</span>
					<a href="#terms">Terms &amp; Conditions</a> <span>|</span>
					<a href="#feedback">Feedback</a>

				</p>
			</div>
		</div>
	</footer>


	<!-- Javascript -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script async src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script async src="<?php bloginfo('template_url'); ?>/assets/js/jquery.swipebox.min.js"></script>
	<script async src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script async src="<?php bloginfo('template_url'); ?>/assets/js/script.min.js?version=<?=time()?>"></script>

	<!-- Is this required? -->
	<!-- script async src="<?php bloginfo('template_url'); ?>/assets/js/jasny-bootstrap.min.js"></script -->

	<!-- Wordpress Footer -->
	<?php wp_footer(); ?>

</body>
</html>
