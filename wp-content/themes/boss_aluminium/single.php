<?php get_header(); ?>
		
		<!-- BANNER -->
		
		<div class="banner">
		<div class="container">
		<div class="bannerwrap">
					
			<?php if(has_post_thumbnail()) :?>
			
			    <?php the_post_thumbnail('thumbnail', array('class' => 'img-responsive')); ?>
			
			<?php else : ?>
			    
			    <img src="<?php bloginfo('template_url'); ?>/assets/images/banners/banner-sm.jpg" class="img-responsive">
			
			<?php endif; ?>
		
		</div><!--/.bannerwrap -->
		</div><!--/.container -->
		</div><!--/.banner -->
		
		
		<!-- CONTENT -->
		
		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">
				
			<div class="col-sm-8">
			<div class="main">
				
				<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>

				<!-- Loop -->
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				
					<h1><?php the_title(); ?></h1>

					<div class="meta">Posted on: <?php the_time('F j Y') ?></div>
					<div class="share">
						<ul class="list-inline list-social list-share">
							<li>Share:</li>
							<li><a class="btn btn-social btn-facebook" href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<li><a class="btn btn-social btn-twitter" href="http://twitter.com/share?url=<?php echo get_permalink(); ?>&text=<?php the_title(); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<li><a class="btn btn-social btn-google-plus" href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
							<li><a class="btn btn-social btn-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							<li><a class="btn btn-social btn-envelope" href="mailto:?Subject=Simple Share Buttons&Body=I%20saw%20this%20and%20thought%20of%20you!%20 <?php echo get_permalink(); ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
						</ul>
					</div>

					<?php the_content(); ?>
				
				<?php endwhile; ?>
				<!-- END Loop -->
			
			</div> 
			</div><!--/.main -->
			
			<div class="col-sm-4">
			<div class="aside">
				
				<?php get_sidebar(); ?>
				
			</div>			    
			</div><!--/.aside -->
				
		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->
		
<?php get_footer(); ?>