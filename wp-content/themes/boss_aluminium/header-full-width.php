<!DOCTYPE html>
<html lang="en">
<head>

    <title><?php wp_title(); ?></title>

    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <!-- Icons -->
    <link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
    <link rel="apple-touch-startup-image" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-touch-icon-57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-touch-icon-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-touch-icon-114.png" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?a=03" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/swipebox.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/jasny-bootstrap.min.css">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.swipebox.min.js"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.placeholder.js"></script>
    <![endif]-->

    <script>
      // IE @media fix
      if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style")
        msViewportStyle.appendChild(
          document.createTextNode(
            "@-ms-viewport{width:auto!important}"
          )
        )
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
      }

      // Compress Header
      <?php
        $scroll_top = 0;
        if (is_front_page()) {
          $scroll_top = 50;
        }
      ?>
      var scroll_top = <?php echo $scroll_top; ?>; 
      var bannerHeight = 1000; 
      var menuHeight = 100;

      $(document).scroll(function() {
        // Scroll to bottom of banner
        // menuHeight = $('.header').height() + 1; // +1 forces the heading transition
        // if ($(document).scrollTop() > bannerHeight - menuHeight) { }
        if ($(document).scrollTop() >= scroll_top) {
          $('.desktop-nav').addClass('alt-header');
        } else {
          $('.desktop-nav').removeClass('alt-header');
        }
      });

    </script>

    <!-- Wordpress Header -->
    <?php wp_head(); ?>

</head>

<body <?php body_class($class); ?>>

  <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
  <![endif]-->

  <a href="#content" class="sr-only">Skip to main content</a>

  <div class="wrap">
    <!-- HEADER -->
    <div class="hidden-xs desktop-nav<?php if ($scroll_top == 0) echo ' alt-header' ?>">
      <div class="header">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a class="navbar-brand" href="#">
            <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/clientname-logo.png" alt="<?php bloginfo('name'); ?>" class="img-responsive" ></a>
          </a>
          <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="navbar-collapse collapse" id="navbarCollapse" style="">
            <?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '<ul class="mr-auto navbar-nav">%3$s</ul>', ) ); ?>
           <!--  <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
              </li>
            </ul> -->
            <form class="form-inline mt-2 mt-md-0">
              <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
        </nav>
      <?php if (is_front_page()) { ?>
      <!-- Press to Call -->
      <div class="col-xs-12 visible-xs presstocall">
        <a class="btn btn-block btn-action btn-presstocall clearfix" href="tel:040X XXX XXX"><span class="pull-left">Press to call</span> <span class="pull-right">040X XXX XXX</span></a>
      </div>
      <?php } ?>
    </div>
