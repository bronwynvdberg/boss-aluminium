
<?php query_posts( array( 'post_type' => ‘testimonials’, 'showposts' => 1, 'orderby' => rand) );
  if ( have_posts() ) : while ( have_posts() ) : the_post();

  	$testimoniallong=get_post_meta($post->ID, 'testimonial_short', true);
	$testimonialperson=get_post_meta($post->ID, 'testimonial_person', true);
	$testimoniallocation=get_post_meta($post->ID, 'testimonial_location', true);

?>

		<div class="" itemscope itemtype="http://schema.org/Review">

			<p itemprop="reviewBody"><?php the_field('testimonial_short'); ?></p>
			<?php if ( $testimonialperson ) : ?><span class="person" itemprop="author" itemscope itemtype="http://schema.org/Person"><spanitempr"name"><?php the_field('testimonial_person'); ?></span><?php endif; ?>
			<?php if ( $testimoniallocation ) : ?>, <?php the_field('testimonial_location'); ?><?php endif; ?>
			<?php if ( $testimonialperson ) : ?></span><?php endif; ?>
	
		</div>
			    				
<?php endwhile; endif; wp_reset_query(); ?>